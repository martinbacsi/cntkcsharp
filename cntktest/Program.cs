﻿
using System;
using CNTK;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Emgu.CV;
using Emgu.CV.Structure;

namespace CNTKLibraryCSEvalExamples
{
    class Program
    {
        const int hStride = 1;
        const int vStride = 1;
        const double wScale = 1.0;
        const double bValue = 1.0;
        const double scValue = 1.0;
        const int bnTimeConst = 4096;
        const bool spatial = true;
        const string modelFile = "MNISTConvolution.model";

        const int inSize = 4096;
        const int outSize = 2048;

        public static void EvaluationSingleImage(DeviceDescriptor device)
        {
            try
            {
                Function modelFunc = Function.Load(modelFile, device);
                Variable inputVar = modelFunc.Arguments.Single();
                NDShape inputShape = inputVar.Shape;
                string sampleImage = ImageDataFolder +   "testin2.png";
                Image<Gray, byte> img = new Image<Gray, byte>(sampleImage);
                var imgFloat = img.Bytes.Select(x => (float)x * 0.00390625f).ToList();

                // Create input data map
                var inputDataMap = new Dictionary<Variable, Value>();
                var inputVal = Value.CreateBatch(inputShape, imgFloat, device);
                inputDataMap.Add(inputVar, inputVal);

                Variable outputVar = modelFunc.Output;
                var outputDataMap = new Dictionary<Variable, Value>()
                {
                    { outputVar, null }
                };

                modelFunc.Evaluate(inputDataMap, outputDataMap, device);

                var outputVal = outputDataMap[outputVar];
                var outputData = outputVal.GetDenseData<float>(outputVar);

                Image<Gray, byte> imgke = new Image<Gray, byte>(outSize, outSize);
                var dataBytes = outputData[0].Select(x => (byte)(x * 255)).ToArray();

                byte[] kakkukk = new byte[outSize * outSize];
                for (int i = 0; i < outSize * outSize; ++i)
                {
                    kakkukk[i] = (byte)(dataBytes[i]);
                }
                imgke.Bytes = kakkukk;
                imgke.Save(ImageDataFolder + "kakukk.png");
                for (int i = 0; i < outSize * outSize; ++i)
                {
                    kakkukk[i] = (byte)(dataBytes[outSize * outSize + i]);
                }
                imgke.Bytes = kakkukk;


                imgke.Save(ImageDataFolder + "kakukk2.png");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: {0}\nCallStack: {1}\n Inner Exception: {2}", ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "No Inner Exception");
                throw ex;
            }
        }

        static Function CreateNN(Variable features, int outDims, DeviceDescriptor device, string classifierName)
        {
            Function conv1 = CNTKLib.ReLU(ConvBatchNorm(features, 1, 3,  device));
            Function conv2 = CNTKLib.ReLU(ConvBatchNorm(conv1, 1, 3,  device));
            Function conv3 = CNTKLib.Sigmoid(Conv(conv2, 1, 3,  device));

            return CNTKLib.Pooling(conv3, PoolingType.Max,
               new int[] { 3, 3, 1 }, new int[] { 2, 2, 1 }, new bool[] { true });
        }

        private  static Function Conv(Variable input, int outFeatureMapCount, int kernel, DeviceDescriptor device)
        {
            int numInputChannels = input.Shape[input.Shape.Rank - 1];
            var convParams = new Parameter(new int[] { kernel, kernel, numInputChannels, outFeatureMapCount },
                DataType.Float, CNTKLib.GlorotUniformInitializer(wScale, -1, 2), device);
           return CNTKLib.Convolution(convParams, input, new int[] { hStride, vStride, numInputChannels });
        }

        private static Function ConvBatchNorm(Variable input, int outFeatureMapCount, int kernel, DeviceDescriptor device)
        {
            var convFunction = Conv(input, outFeatureMapCount, kernel, device);
            var biasParams = new Parameter(new int[] { NDShape.InferredDimension }, 1.0f, device, "");
            var scaleParams = new Parameter(new int[] { NDShape.InferredDimension }, 1.0f, device, "");
            var runningMean = new Parameter(new int[] { NDShape.InferredDimension }, 0.0f, device, "");
            var runningInvStd = new Parameter(new int[] { NDShape.InferredDimension }, 0.0f, device, "");
            var runningCount = Constant.Scalar(0.0f, device);
            return CNTKLib.BatchNormalization(convFunction, scaleParams, biasParams, runningMean, runningInvStd, runningCount,
                spatial, (double)bnTimeConst, 0.0, 1e-5 /* epsilon */);
        }

        public static string ImageDataFolder = "../../../test/";

        public static void TrainAndEvaluate(DeviceDescriptor device)
        {
            var featureStreamName = "features";
            var classifierName = "classifierOutput";
            int numClasses = 10;
            // build the network
            var input = CNTKLib.InputVariable(new int[] { inSize, inSize, 1 }, DataType.Float, featureStreamName);
            var target = CNTKLib.InputVariable(new int[] { outSize, outSize, 2 }, DataType.Float, featureStreamName);

            var output = CreateNN(input, numClasses, device, classifierName);

            
            //var outReshape = CNTKLib.Reshape(output, new int[] { outSize * outSize }, new Axis(0), new Axis(2));
            //var targetReshape = CNTKLib.Reshape(target, new int[] {outSize * outSize }, new Axis(0), new Axis(2));


            var trainLoss2 = CNTKLib.ReduceMean(CNTKLib.Square(CNTKLib.Minus(output, target)), Axis.AllAxes());
            TrainingParameterScheduleDouble learningRatePerSample = new TrainingParameterScheduleDouble(
                0.03, 1);

            TrainingParameterScheduleDouble learningMomentum = new TrainingParameterScheduleDouble(
                0.09, 1);
            ParameterVector pv = new ParameterVector();
            foreach (var element in output.Parameters())
                pv.Add(element);
            var trainer = Trainer.CreateTrainer(
                output,
                trainLoss2,
                trainLoss2,
                new List<Learner>
                {
                    CNTKLib.AdamLearner(
                        pv,
                        learningRatePerSample,
                        learningMomentum,
                        false)
                });

 

            Image<Gray, byte> img = new Image<Gray, byte>(ImageDataFolder + "testin.png");
            var imgFloat = img.Bytes.Select(x => (float)x * 0.00390625f).ToList();
            Image<Gray, byte> imgTarget = new Image<Gray, byte>(ImageDataFolder + "testout.png");
            var imgTargetFloat = new float[outSize * outSize * 2];
            var imgTargetBytes = imgTarget.Bytes;
            for (int i = 0; i < imgTargetBytes.Length ; ++i )
            {
                var val = (float)Math.Round(imgTargetBytes[i] * 0.00390625f);
                imgTargetFloat[i] = val;
                imgTargetFloat[outSize * outSize + i] = 1.0f - val;
            }

            int epochs = 1000;

            var inputDataMap = new Dictionary<Variable, Value>();
            var inputVal = Value.CreateBatch(new int[] { inSize, inSize, 1}, imgFloat, device);

            var outputVal = Value.CreateBatch(new int[] { outSize, outSize, 2 }, imgTargetFloat, device);

            inputDataMap.Add(input, inputVal);
            var arguments = new Dictionary<Variable, Value>
            {
                { input, inputVal },
                { target, outputVal }
            };
            for (int i = 0; i < epochs; ++i)              
            {
                trainer.TrainMinibatch(arguments, device);
                if(i % 20 == 0)
                     Console.WriteLine($"eval = {trainer.PreviousMinibatchLossAverage()}");
            }
            output.Save(modelFile);
        }
        static void Main(string[] args)
        {
            TrainAndEvaluate(DeviceDescriptor.GPUDevice(0));
            EvaluationSingleImage(DeviceDescriptor.GPUDevice(0));
        }
    }
}
